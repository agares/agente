from django.conf.urls import patterns, include, url
from django.conf import settings
from core.views import PropertyDetailView, PropertyCreate, UserPropertyList
from core.models import Property

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', UserPropertyList.as_view(template_name='property_list.html'), name='home'),
    #url(r'^$', 'core.views.home', name='home'),
    url(r'^search', 'core.views.home', name='search'),
    url(r'^results', 'core.views.results', name='search-results'),
    url(r'^contactar$', 'core.views.contactar', name='contactar'),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name': 'login.html'}, name='login'),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', {'template_name': 'home.html'}, name='logout'),

    url(r'^property/(?P<pk>\d+)$', PropertyDetailView.as_view(template_name='property_detail.html'), name='property-detail'),
    url(r'^property/add$', PropertyCreate.as_view(template_name='property_form.html'), name='property-add'),
    url(r'^property/list$', UserPropertyList.as_view(template_name='property_list.html'), name='property-list'),
    # url(r'^agente/', include('agente.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    #url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
    
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns
    from django.conf.urls.static import static
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
