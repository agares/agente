from django.shortcuts import render_to_response, redirect
from django.core.urlresolvers import reverse
from django.template import RequestContext
from django.views.generic.edit import CreateView
from django.views.generic.detail import DetailView
from django.views.generic import ListView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

from django.http import Http404

from core.models import Property, PropertyDictionary
import urllib

from models import User

def home(request):
    if request.method == 'POST':
        return redirect('/results?%s' % request.POST.urlencode())

    properties_dictionary = PropertyDictionary.objects.get_all_names()
    return render_to_response('home.html',
            {'properties_dictionary': properties_dictionary},
            context_instance=RequestContext(request))

def results(request):
    results = User.objects.with_available_properties(request.GET)

    return render_to_response('results.html', {'contacts': results},
            context_instance=RequestContext(request))


class UserPropertyList(ListView):
    model = Property

    def get_queryset(self):
        return self.request.user.property_set.all()

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(UserPropertyList, self).dispatch(*args, **kwargs)


class PropertyCreate(CreateView):
    model = Property
    fields = ['name']

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PropertyCreate, self).dispatch(*args, **kwargs)


class PropertyDetailView(DetailView):
    model = Property

    def get_object(self, queryset=None):
        try:
            return self.request.user.property_set.get(pk=self.kwargs['pk'])
        except Property.DoesNotExist:
            raise Http404


    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PropertyDetailView, self).dispatch(*args, **kwargs)

def contactar(request):
    contact_ids = request.POST.getlist('contact')
    return redirect('search')
