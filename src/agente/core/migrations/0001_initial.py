# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'User'
        db.create_table(u'core_user', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('last_login', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('email', self.gf('django.db.models.fields.EmailField')(unique=True, max_length=75)),
            ('names', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('last_names', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=15)),
            ('pic', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
            ('cc', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal(u'core', ['User'])

        # Adding model 'Property'
        db.create_table(u'core_property', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('manager', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.User'])),
            ('property_type', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('zone', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('sector', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('social_bedrooms', self.gf('django.db.models.fields.IntegerField')()),
            ('service_bedrooms', self.gf('django.db.models.fields.IntegerField')()),
            ('social_bathrooms', self.gf('django.db.models.fields.IntegerField')()),
            ('service_bathrooms', self.gf('django.db.models.fields.IntegerField')()),
            ('parking_spaces', self.gf('django.db.models.fields.IntegerField')()),
            ('parking_type', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('area', self.gf('django.db.models.fields.IntegerField')()),
            ('administrative_fee', self.gf('django.db.models.fields.BigIntegerField')()),
            ('antiquity', self.gf('django.db.models.fields.IntegerField')()),
            ('price', self.gf('django.db.models.fields.BigIntegerField')()),
            ('duplex', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('penthouse', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('study_room', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('extra_info', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'core', ['Property'])


    def backwards(self, orm):
        # Deleting model 'User'
        db.delete_table(u'core_user')

        # Deleting model 'Property'
        db.delete_table(u'core_property')


    models = {
        u'core.property': {
            'Meta': {'object_name': 'Property'},
            'administrative_fee': ('django.db.models.fields.BigIntegerField', [], {}),
            'antiquity': ('django.db.models.fields.IntegerField', [], {}),
            'area': ('django.db.models.fields.IntegerField', [], {}),
            'duplex': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'extra_info': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manager': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.User']"}),
            'parking_spaces': ('django.db.models.fields.IntegerField', [], {}),
            'parking_type': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'penthouse': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'price': ('django.db.models.fields.BigIntegerField', [], {}),
            'property_type': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'sector': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'service_bathrooms': ('django.db.models.fields.IntegerField', [], {}),
            'service_bedrooms': ('django.db.models.fields.IntegerField', [], {}),
            'social_bathrooms': ('django.db.models.fields.IntegerField', [], {}),
            'social_bedrooms': ('django.db.models.fields.IntegerField', [], {}),
            'study_room': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'zone': ('django.db.models.fields.CharField', [], {'max_length': '2'})
        },
        u'core.user': {
            'Meta': {'object_name': 'User'},
            'cc': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_names': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'names': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'pic': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'})
        }
    }

    complete_apps = ['core']