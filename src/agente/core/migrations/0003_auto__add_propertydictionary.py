# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'PropertyDictionary'
        db.create_table(u'core_propertydictionary', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('value', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('clean_name', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('clean_value', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('the_property', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Property'])),
        ))
        db.send_create_signal(u'core', ['PropertyDictionary'])


    def backwards(self, orm):
        # Deleting model 'PropertyDictionary'
        db.delete_table(u'core_propertydictionary')


    models = {
        u'core.property': {
            'Meta': {'object_name': 'Property'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'administrative_fee': ('django.db.models.fields.BigIntegerField', [], {}),
            'antiquity': ('django.db.models.fields.IntegerField', [], {}),
            'area': ('django.db.models.fields.IntegerField', [], {}),
            'duplex': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'extra_info': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manager': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.User']"}),
            'parking_spaces': ('django.db.models.fields.IntegerField', [], {}),
            'parking_type': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'penthouse': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'price': ('django.db.models.fields.BigIntegerField', [], {}),
            'property_type': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'sector': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'service_bathrooms': ('django.db.models.fields.IntegerField', [], {}),
            'service_bedrooms': ('django.db.models.fields.IntegerField', [], {}),
            'social_bathrooms': ('django.db.models.fields.IntegerField', [], {}),
            'social_bedrooms': ('django.db.models.fields.IntegerField', [], {}),
            'study_room': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'zone': ('django.db.models.fields.CharField', [], {'max_length': '2'})
        },
        u'core.propertydictionary': {
            'Meta': {'object_name': 'PropertyDictionary'},
            'clean_name': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'clean_value': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'the_property': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Property']"}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '500'})
        },
        u'core.user': {
            'Meta': {'object_name': 'User'},
            'cc': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_names': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'names': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'pic': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'})
        }
    }

    complete_apps = ['core']