# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User, UserManager, AbstractBaseUser

from django.core.urlresolvers import get_script_prefix, reverse
from django.conf import settings

def join_absolute_url(*st_path):
    """ Join path html"""
    if len(st_path)>0 and "http" in st_path[0]:
        base_url=st_path[0].strip("/")
        st_path=st_path[1:]
    else:
        base_url=get_script_prefix().strip("/")
        
    return '/'.join([base_url] + [i.strip("/") for i in st_path])


def get_absolute_url(*st_path):
    return join_absolute_url(settings.MEDIA_URL,*st_path)


class MyUserManager(UserManager):

    def create_user(self, username, email, password=None):
        user = self.model(email=email)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password):
        user = self.create_user(username, '', password)
        user.is_admin = True
        user.is_superuser = True
        user.save(using=self._db)
        return user
    
    def with_available_properties(self, query):
        zone = query.get('zone')
        sector = query.get('sector')
        social_bedrooms = query.get('social-bedrooms')
        service_bedrooms = query.get('service-bedrooms')
        social_bathrooms = query.get('social-bathrooms')
        service_bathrooms = query.get('service-bathrooms')
        parking_spaces = query.get('parking-spaces')
        parking_type = query.get('parking-type')
        area = query.get('area')
        administrative_fee = query.get('administrative-fee')
        antiquity = query.get('antiquity')
        price = query.get('price')
        duplex = query.get('duplex')
        penthouse = query.get('penthouse')
        study_room = query.get('study-room')

        records = Property.objects.all()
        if zone:
            records = records.filter(zone=zone)
        if sector:
            records = records.filter(sector=sector)
        if social_bedrooms:
            records = records.filter(social_bedrooms=social_bedrooms)
        if service_bedrooms:
            records = records.filter(service_bedrooms=service_bedrooms)
        if social_bathrooms:
            records = records.filter(social_bathrooms=social_bathrooms)
        if service_bathrooms:
            records = records.filter(service_bathrooms=service_bathrooms)
        if parking_spaces:
            records = records.filter(parking_spaces=parking_spaces)
        if parking_type:
            records = records.filter(parking_type=parking_type)
        if area:
            records = records.filter(area=area)
        if administrative_fee:
            records = records.filter(administrative_fee=administrative_fee)
        if antiquity:
            records = records.filter(antiquity=antiquity)
        if price:
            records = records.filter(price=price)
        if duplex:
            records = records.filter(duplex=duplex)
        if penthouse:
            records = records.filter(penthouse=penthouse)
        if study_room:
            records = records.filter(study_room=study_room)

        managers = {}
        id = -1
        for property in records:
            id = property.manager.id
            if id not in managers:
                managers[id] = property.manager
                managers[id].found_properties = []
            
            managers[id].found_properties.append(property.id)

        results = []
        for manager in managers:
            results.append(managers[manager])

        return results


class User(AbstractBaseUser):
    email = models.EmailField(unique=True)
    names = models.CharField(max_length=32)
    last_names = models.CharField(max_length=32)
    phone = models.CharField(max_length=15)
    pic = models.ImageField(upload_to='users/', blank=True)
    cc = models.CharField(max_length=20)

    USERNAME_FIELD = 'email'

    objects = MyUserManager()

    def is_staff(self):
        return True

    def has_module_perms(self, app_label):
        print app_label
        return True

    def has_perm(self, perm, obj=None):
        print '-------'
        print '%s %s' % (perm, obj) 
        print '-------'
        return True

    def get_short_name(self):
        return self.names

    def get_username(self):
        return self.names

    @property
    def image_url(self):
        return get_absolute_url(self.pic.name)


    def full_name(self):
        return u'%s %s' % (self.names, self.last_names)

    def __unicode__(self):
        return self.full_name()


TYPE_PROPERTY_CHOICES = (('ap','Apartamento'),
                         ('ca','Casa'),
                         ('of','Oficina'),
                         ('bo','Bodega'))

ZONE_PROPERTY_CHOICES = (('N', 'Norte'),
                         ('No', 'Noroccidente'),
                         ('O', 'Occidental'),
                         ('C', 'Chapinero'),
                         ('G', 'Guaymaral'),
                         ('C', 'Centro'),
                         ('S', 'Sur'))

PARKING_TYPES = (('I', 'Independiente'),
                 ('S', 'En servidumbre'))

class Property(models.Model):
    manager = models.ForeignKey(User, verbose_name='Agente')
    property_type = models.CharField(max_length=2, verbose_name='Tipo de Propiedad', choices=TYPE_PROPERTY_CHOICES)
    zone = models.CharField(max_length=2, verbose_name='Zona', choices=ZONE_PROPERTY_CHOICES)
    sector = models.CharField(max_length=500, verbose_name='Sector')
    social_bedrooms = models.IntegerField(verbose_name='Alcobas')
    service_bedrooms = models.IntegerField(verbose_name='Alcobas de Servicio')
    social_bathrooms = models.IntegerField(verbose_name='Baños sociales')
    service_bathrooms = models.IntegerField(verbose_name='Baños de Servicio')
    parking_spaces = models.IntegerField(verbose_name='Parqueaderos')
    parking_type = models.CharField(max_length=1, verbose_name='Tipo de parqueadero', choices=PARKING_TYPES)
    area = models.IntegerField(verbose_name='Área')
    administrative_fee = models.BigIntegerField(verbose_name='Administración')
    antiquity = models.IntegerField(verbose_name='Antigüedad')
    price = models.BigIntegerField(verbose_name='Precio')
    duplex = models.BooleanField(default=False, verbose_name='Duplex')
    penthouse = models.BooleanField(default=False, verbose_name='Penthouse')
    study_room = models.BooleanField(default=False, verbose_name='Cuarto de estudio')
    extra_info = models.TextField(blank=True, verbose_name='Información Adicional')
    address = models.CharField(max_length=500, verbose_name='Direccion')

    def __unicode__(self):
        return u'%s en %s. valor: %s' % (self.property_type, self.sector, self.price)

    def get_absolute_url(self):
        return reverse('property-detail', kwargs={'pk': self.pk})

class PropertyDictionaryManager(models.Manager):
    def get_all_names(self):
        return self.all()

class PropertyDictionary(models.Model):
    name = models.CharField(max_length=500, verbose_name='Nombre')
    value = models.CharField(max_length=500, verbose_name='Valor')
    clean_name = models.CharField(max_length=500)
    clean_value = models.CharField(max_length=500)
    the_property = models.ForeignKey(Property, related_name='value_dictionary')
    objects = PropertyDictionaryManager()

    def __unicode__(self):
        return u'%s: %s' % (self.name, self.value)
